import React from 'react';

interface BlueprintContextState {
  theme: string;
}

const initialBlueprintContextState: BlueprintContextState = {
  theme: 'bp3-dark'
};

const BlueprintContext = React.createContext(initialBlueprintContextState);

export const BlueprintProvider: React.FC = ({ children }) => (
  <BlueprintContext.Provider value={initialBlueprintContextState}>
    {children}
  </BlueprintContext.Provider>
);

export const useBlueprint = () => {
  return React.useContext(BlueprintContext);
};
