import React from 'react';
import { ApolloProvider as ApolloProviderComponent } from '@apollo/react-hooks';
import ApolloClient from 'apollo-boost';
import { Operation } from 'apollo-link';
import { ErrorResponse } from 'apollo-link-error';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { Intent, H4 } from '@blueprintjs/core';
import { LocalStorageKeys } from '../models';
import { showNotification } from '../common';

const cache = new InMemoryCache();

const initialStore = {
  data: { token: localStorage.getItem(LocalStorageKeys.TOKEN) }
};

cache.writeData(initialStore);

export const client = new ApolloClient({
  cache,
  uri: 'http://localhost:4000/graphql',
  request: (op: Operation) => {
    console.log('GQL req: ', op);
    const token = localStorage.getItem(LocalStorageKeys.TOKEN);
    op.setContext({
      headers: {
        authorization: token ? `Bearer ${token}` : ''
      }
    });
  },
  onError: (error: ErrorResponse) => {
    console.error('Error =>',error);
    error.response && error.response.errors!.forEach((error) => {
      showNotification({
        intent: Intent.DANGER,
        message: <div className='bp3-dark'>
          <H4>Error!</H4>
          {error.message}
        </div>,
        icon: 'error',
        timeout: 8000
      });
    });

    if(error.networkError){
      showNotification({
        intent: Intent.DANGER,
        message: <div className='bp3-dark'>
          <H4>Connectivity error!</H4>
          {error.networkError.message}
        </div>,
        icon: 'error',
        timeout: 5000
      });
    }
  },
  name: 'boilerplate-apollo-client',
  version: '1.0.0'
});

export const ApolloProvider: React.FC = ({ children }) => (
  <ApolloProviderComponent client={client}>{children}</ApolloProviderComponent>
);
