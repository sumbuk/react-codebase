import React, { useCallback } from 'react';
import { Intent, Button, Classes, Dialog, Icon } from '@blueprintjs/core';

export interface ConfirmationProps {
  isOpen: boolean;
  setOpen: (value: boolean) => void;
  intent: Intent;
  icon?: string;
  title: string;
  onCancel?: () => void;
  onConfirm: () => void;
}

export const Confirmation: React.FC<ConfirmationProps> = ({
  isOpen,
  setOpen,
  icon,
  intent,
  title,
  onCancel,
  onConfirm,
  children
}) => {
  const handleCancel = useCallback(()=>{
    setOpen(false);
    onCancel && onCancel();
  },[onCancel, setOpen]);
  const handleConfirm = useCallback(()=>{
    setOpen(false);
    onConfirm();
  },[onConfirm, setOpen]);
  return (
    <Dialog
      className="bp3-dark"
      isOpen={isOpen}
      icon={<Icon icon={icon as any} intent={intent}/>}
      onClose={handleCancel}
      title={title}
      autoFocus
      canEscapeKeyClose
      canOutsideClickClose
      enforceFocus
      usePortal
    >
      <div className={Classes.DIALOG_BODY}>{children}</div>
      <div className={Classes.DIALOG_FOOTER}>
        <div className={Classes.DIALOG_FOOTER_ACTIONS}>
          <Button onClick={handleCancel}>Cancel</Button>
          <Button intent={intent} onClick={handleConfirm}>Ok</Button>
        </div>
      </div>
    </Dialog>
  );
};
