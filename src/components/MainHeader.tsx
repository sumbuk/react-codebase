import React, { useState, useMemo, useCallback } from 'react';
import { Row, Col } from 'react-flexbox-grid';
import { isMobile } from 'react-device-detect';
import { useHistory } from 'react-router-dom';
import { useApolloClient } from '@apollo/react-hooks';
import { Navbar, Button, Alignment, Intent, Classes, Drawer, Position } from '@blueprintjs/core';
import { routes } from '../router';
import { useButton, useToken } from '../hooks';
import { useBlueprint } from '../providers';
import { LocalStorageKeys } from '../models';
import { showNotification } from '../common';
import { Confirmation } from '../components/common/Confirmation';
import styles from '../css/MainHeader.module.css';

interface MainHeaderProps {}

const rowGroup = (node: React.ReactNode) => (
  <Row start="xs">
    <Col xs>{node}</Col>
  </Row>
);

const navbarWrapper = (node: React.ReactNode, theme: string) => (
  <header className={styles.navbar + ' ' + theme}>{node}</header>
);

const MainHeader: React.FC<MainHeaderProps> = () => {
  const client = useApolloClient();
  const token = useToken();
  const history = useHistory();
  const { theme } = useBlueprint();
  const [isLogoutDialogOpened, setLogoutDialogOpened] = useState(false);
  const [isSidebarOpened, setSidebarOpened] = useState(false);

  const logout = useCallback(() => {
    localStorage.removeItem(LocalStorageKeys.TOKEN);
    client!.writeData({
      data: { token: '' }
    });
    history.replace(routes.home);
    showNotification({
      intent: Intent.WARNING,
      message: 'Goodbye old friend!',
      icon: 'hand'
    });
  }, [client, history]);

  const bindToHomeButton = useButton(() => history.push(routes.home), [], 'Home', 'home');
  const bindToRegisterButton = useButton(
    () => history.push(routes.register), 
    [],
    'Sign up',
    'new-person'
  );
  const bindToLoginButton = useButton(() => history.push(routes.login), [], 'Sign in', 'log-in');
  const bindLogoutDialogButton = useButton(
    () => setLogoutDialogOpened(true), 
    [],
    'Log out',
    'log-out'
  );

  const logoutConfirmation = useMemo(
    () => (
      <Confirmation
        isOpen={isLogoutDialogOpened}
        setOpen={setLogoutDialogOpened}
        icon="warning-sign"
        intent={Intent.WARNING}
        title="Logout complete..."
        onConfirm={logout}
      >
        Are you sure?
      </Confirmation>
    ),
    [isLogoutDialogOpened, logout]
  );

  if (isMobile) {
    return navbarWrapper(
      <>
        <Navbar>
          <Navbar.Group align={Alignment.LEFT}>
            <Button
              minimal
              icon="menu"
              onClick={() => setSidebarOpened(true)}
              className={styles.menuButton}
            />
          </Navbar.Group>
          <Navbar.Group align={Alignment.CENTER}>
            <Navbar.Heading className={styles.centered}>Header</Navbar.Heading>
          </Navbar.Group>
        </Navbar>
        <Drawer
          className={theme}
          onClose={() => setSidebarOpened(false)}
          isOpen={isSidebarOpened}
          position={Position.LEFT}
          size="auto"
          title={
            <div style={{ height: 40, display: 'flex', alignItems: 'center' }}>Sidebar menu</div>
          }
          autoFocus
          canEscapeKeyClose
          canOutsideClickClose
          enforceFocus
          hasBackdrop
          usePortal
        >
          <div className={Classes.DRAWER_BODY}>
            <div className={Classes.DIALOG_BODY}>
              {token ? (
                rowGroup(<Button {...bindLogoutDialogButton} />)
              ) : (
                <>
                  {rowGroup(<Button {...bindToLoginButton} />)}
                  {rowGroup(<Button {...bindToRegisterButton} />)}
                </>
              )}
              {rowGroup(<Button {...bindToHomeButton} />)}
            </div>
          </div>
          <div className={Classes.DRAWER_FOOTER}>Footer</div>
        </Drawer>
        {logoutConfirmation}
      </>,
      theme
    );
  }

  return navbarWrapper(
    <>
      <Navbar>
        <Navbar.Group align={Alignment.LEFT}>
          <Navbar.Heading>Header</Navbar.Heading>
        </Navbar.Group>
        <Navbar.Group align={Alignment.RIGHT}>
          <Navbar.Divider />
          {<Button {...bindToHomeButton} />}
          <Navbar.Divider />
          {token ? (
            <Button {...bindLogoutDialogButton} />
          ) : (
            <>
              {<Button {...bindToRegisterButton} />}
              {<Button {...bindToLoginButton} />}
            </>
          )}
        </Navbar.Group>
      </Navbar>
      {logoutConfirmation}
    </>,
    theme
  );
};

export default MainHeader;
