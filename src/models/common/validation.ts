export class ValidationResult {
  isValid: boolean;
  errText: string;
  constructor(errText?: string) {
    this.isValid = !errText;
    this.errText = errText || '';
  }
}

export enum FieldType {
  FULLNAME = 'fullname',
  EMAIL = 'email',
  PASSWORD = 'password'
}

export interface ValidateArgs {
  type: FieldType;
  value: string;
  setter?: (value: string) => void;
  required?: boolean;
  rule?: {
    test: () => boolean;
    errText: string;
  };
}
