import React from 'react';
import { H1 } from '@blueprintjs/core';
import BaseLayout from '../layouts/BaseLayout';
import MainHeader from '../components/MainHeader';
import logo from '../logo.svg';

const Home: React.FC = () => {
  return (
    <>
      <MainHeader />
      <BaseLayout>
        <div className="App">
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
          </header>
          <H1>Home page</H1>
        </div>
      </BaseLayout>
    </>
  );
};

export default Home;
