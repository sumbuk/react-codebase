import React, { useCallback, useEffect } from 'react';
import { Row, Col } from 'react-flexbox-grid';
import { Card, Elevation, Button, H2, FormGroup, InputGroup, Intent } from '@blueprintjs/core';
import { useHistory } from "react-router-dom";
import { RouterProps } from 'react-router';
import { routes } from '../router';
import AuthLayout from '../layouts/AuthLayout';
import { useInput } from '../hooks';
import { validate, showNotification } from '../common';
import { FieldType, LocalStorageKeys } from '../models';
import MainHeader from '../components/MainHeader';
import { useAuthenticateUserMutation } from '../gql';

interface LoginProps extends RouterProps {}

const rowGroup = (node: React.ReactNode) => (
  <Row start="xs">
    <Col xs>{node}</Col>
  </Row>
);

const Login: React.FC<LoginProps> = () => {
  const history = useHistory();
  const { value: username, validation: validUsername, bind: bindUsername } = useInput('');
  const {
    value: password,
    validation: validPassword,
    bind: bindPassword,
    reset: resetPassword
  } = useInput('');

  const [login, { data, error, loading, client }] = useAuthenticateUserMutation();
  useEffect(() => {
    if (error) {
      resetPassword();
    }
  }, [error]);
  useEffect(() => {
    if (data && data.authenticateUser) {
      localStorage.setItem(LocalStorageKeys.TOKEN, data.authenticateUser);
      client!.writeData({
        data: { token: data.authenticateUser }
      });
      history.replace(routes.home);
      showNotification({
        intent: Intent.SUCCESS,
        message: 'Successfully logged in',
        icon: 'endorsed'
      });
    }
  }, [data, client, history]);

  const loginHandler = useCallback(() => {
    const result = validate([
      {
        value: username,
        type: FieldType.EMAIL,
        setter: validUsername.setter,
        required: true
      },
      {
        value: password,
        type: FieldType.PASSWORD,
        setter: validPassword.setter,
        required: true
      }
    ]);
    if (result) {
      login({
        variables: {
          email: username,
          password
        }
      });
    }
  }, [username, password, validPassword, validUsername, login]);

  return (
    <>
      <MainHeader />
      <AuthLayout>
        <Card interactive elevation={Elevation.TWO}>
          <H2>Login Form</H2>
          {rowGroup(
            <FormGroup
              helperText={validUsername.text}
              intent={validUsername.intent}
              label={'Username'}
              labelFor="email"
              labelInfo={'*'}
            >
              <InputGroup
                id="email"
                type="email"
                leftIcon="envelope"
                placeholder="Enter your username..."
                {...bindUsername}
              />
            </FormGroup>
          )}
          {rowGroup(
            <FormGroup
              helperText={validPassword.text}
              intent={validPassword.intent}
              label={'Password'}
              labelFor="password"
              labelInfo={'*'}
            >
              <InputGroup
                id="password"
                type="password"
                leftIcon="key"
                placeholder="Enter your password..."
                {...bindPassword}
              />
            </FormGroup>
          )}
          {rowGroup(<Button loading={loading} icon="log-in" text="login" onClick={loginHandler} />)}
        </Card>
      </AuthLayout>
    </>
  );
};

export default Login;
