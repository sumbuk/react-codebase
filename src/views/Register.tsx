import React, { useCallback, useEffect } from 'react';
import { Row, Col } from 'react-flexbox-grid';
import { Card, Elevation, Button, H2, H4, FormGroup, InputGroup, Intent } from '@blueprintjs/core';
import { useHistory } from "react-router-dom";
import AuthLayout from '../layouts/AuthLayout';
import MainHeader from '../components/MainHeader';
import { useInput } from '../hooks';
import { validate, validationText, showNotification } from '../common';
import { FieldType } from '../models';
import { routes } from '../router';
import { useBlueprint } from '../providers';
import { useRegisterUserMutation } from '../gql';

const rowGroup = (node: React.ReactNode) => (
  <Row start="xs">
    <Col xs>{node}</Col>
  </Row>
);

const Register: React.FC = () => {
  const { theme } = useBlueprint();
  const history = useHistory();
  const fullname = useInput('');
  const email = useInput('');
  const password = useInput('');
  const passwordConfirm = useInput('');

  const [register, { data, loading, error }] = useRegisterUserMutation();

  useEffect(() =>{
    if(data && data.registerUser){
      history.replace(routes.login);
      showNotification({
        intent: Intent.SUCCESS,
        message: <div className={theme}>
          <H4>Successfully registered!</H4>
          Now you have to login ...
        </div>,
        icon: 'new-person',
        timeout: 10000
      });
    }
  },[data, history, theme]);
  useEffect(()=>{
    if(error){
      password.reset();
      passwordConfirm.reset();
    }
  },[error]);

  const registerHandler = useCallback(() => {
    const result = validate([
      {
        value: fullname.value,
        type: FieldType.FULLNAME,
        setter: fullname.validation.setter,
        required: true
      },
      {
        value: email.value,
        type: FieldType.EMAIL,
        setter: email.validation.setter,
        required: true
      },
      {
        value: password.value,
        type: FieldType.PASSWORD,
        setter: password.validation.setter,
        required: true
      },
      {
        value: passwordConfirm.value,
        rule: {
          test: () => password.value === passwordConfirm.value,
          errText: validationText.passwordsMissmatch
        },
        type: FieldType.PASSWORD,
        setter: passwordConfirm.validation.setter,
        required: true
      }
    ]);
    if (result) {
      register({
        variables: {
          name: fullname.value,
          email: email.value,
          password: password.value
        }
      });
    }
  }, [fullname, email, password, passwordConfirm, register]);

  return (
    <>
      <MainHeader />
      <AuthLayout>
        <Card interactive elevation={Elevation.TWO}>
          <H2>Register Form</H2>
          {rowGroup(
            <FormGroup
              helperText={fullname.validation.text}
              intent={fullname.validation.intent}
              label={'Fullname'}
              labelFor="fullname"
              labelInfo={'*'}
            >
              <InputGroup
                id="fullname"
                type="text"
                leftIcon="person"
                placeholder="Enter your fullname..."
                {...fullname.bind}
              />
            </FormGroup>
          )}
          {rowGroup(
            <FormGroup
              helperText={email.validation.text}
              intent={email.validation.intent}
              label={'Email'}
              labelFor="email"
              labelInfo={'*'}
            >
              <InputGroup
                id="email"
                type="email"
                leftIcon="envelope"
                placeholder="Enter your email..."
                {...email.bind}
              />
            </FormGroup>
          )}
          {rowGroup(
            <FormGroup
              helperText={password.validation.text}
              intent={password.validation.intent}
              label={'Password'}
              labelFor="password"
              labelInfo={'*'}
            >
              <InputGroup
                id="password"
                type="password"
                leftIcon="key"
                placeholder="Enter your password..."
                {...password.bind}
              />
            </FormGroup>
          )}
          {rowGroup(
            <FormGroup
              helperText={passwordConfirm.validation.text}
              intent={passwordConfirm.validation.intent}
              label={'Password Confirmation'}
              labelFor="passwordConfirmation"
              labelInfo={'*'}
            >
              <InputGroup
                id="passwordConfirmation"
                type="password"
                leftIcon="key"
                placeholder="Confirm your password..."
                {...passwordConfirm.bind}
              />
            </FormGroup>
          )}
          {rowGroup(
            <Button loading={loading} icon="new-person" text="Register" onClick={registerHandler} />
          )}
        </Card>
      </AuthLayout>
    </>
  );
};

export default Register;
