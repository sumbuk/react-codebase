import React from 'react';
import { Position, Toaster, Intent } from '@blueprintjs/core';

export interface NotificationPayload {
  intent: Intent;
  message: React.ReactNode;
  icon: any;
  timeout?: number;
}

/** Singleton toaster instance. Create separate instances for different options. */
const AppToaster = Toaster.create({
  className: 'main-toaster',
  position: Position.TOP_RIGHT,
  canEscapeKeyClear: true
});

export const showNotification = (payload: NotificationPayload) => {
  AppToaster.show({...payload});
};


