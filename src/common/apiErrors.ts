export const apiErrors = {
  unknown: 'Unknown error occurred!',
  noResponse: 'No response from server!',
};
