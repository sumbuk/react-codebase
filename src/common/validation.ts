import { ValidationResult, ValidateArgs } from '../models';

const regexPatterns = {
  fullname: /^([a-zA-Z0-9]+|[a-zA-Z0-9]+\s{1}[a-zA-Z0-9]{1,}|[a-zA-Z0-9]+\s{1}[a-zA-Z0-9]{3,}\s{1}[a-zA-Z0-9]{1,})$/,
  email: /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/,
  password: /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/
};

export const validationText = {
  email: 'Please, enter a valid email',
  fullname: 'Please, enter a valid fullname',
  password: 'Password must contain minimum eight characters, at least one letter and one number',
  passwordsMissmatch: 'Passwords are not equal!'
};

export function isStringEmpty(value: string): boolean {
  return value.length < 1;
}

export function isRequiredFieldValid(args: ValidateArgs): ValidationResult {
  if (isStringEmpty(args.value)) {
    return new ValidationResult('Field cannot be empty!');
  } else {
    return isFieldValid(args);
  }
}

export function isFieldValid(args: ValidateArgs): ValidationResult {
  if (isStringEmpty(args.value)) {
    return new ValidationResult();
  }
  return regexPatterns[args.type].test(args.value)
    ? new ValidationResult()
    : new ValidationResult(validationText[args.type]);
}

export function validate(args: ValidateArgs[]): boolean {
  let result = true;
  for (let i = 0; i < args.length; i++) {
    let validationResult;

    // 1st staging: custom rule
    if(args[i].rule){
      const isPassed = args[i].rule!.test();
      validationResult = new ValidationResult(isPassed ? undefined : args[i].rule!.errText);
    }

    // 2nd staging: default rules by field type
    if((validationResult && validationResult.isValid) || !validationResult){ // if 1st staging is passed
      if (args[i].required) {
        validationResult = isRequiredFieldValid(args[i]);
      } else {
        validationResult = isFieldValid(args[i]);
      }
    }

    // set errors to form or clear them
    if (validationResult.isValid) {
      args[i].setter!('');
    } else {
      result = false;
      args[i].setter!(validationResult.errText);
    }
  }

  return result;
}
