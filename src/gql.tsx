import gql from 'graphql-tag';
import * as ApolloReactCommon from '@apollo/react-common';
import * as React from 'react';
import * as ApolloReactComponents from '@apollo/react-components';
import * as ApolloReactHoc from '@apollo/react-hoc';
import * as ApolloReactHooks from '@apollo/react-hooks';
export type Maybe<T> = T | null;
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string,
  String: string,
  Boolean: boolean,
  Int: number,
  Float: number,
};

export type Cat = {
   __typename?: 'Cat',
  id?: Maybe<Scalars['Int']>,
  name?: Maybe<Scalars['String']>,
  age?: Maybe<Scalars['Int']>,
};

export type Conv2dArgs = {
   __typename?: 'Conv2dArgs',
  activation?: Maybe<Scalars['String']>,
  filters?: Maybe<Scalars['Int']>,
  kernelSize?: Maybe<Scalars['Int']>,
  padding?: Maybe<Scalars['String']>,
};

export type CreateCatInput = {
  name?: Maybe<Scalars['String']>,
  age?: Maybe<Scalars['Int']>,
};

export type CreateLayerInput = {
  name: Scalars['String'],
};

export type CreateUserInput = {
  name: Scalars['String'],
  email: Scalars['String'],
  password: Scalars['String'],
};

export enum DataState {
  DataNone = 'DATA_NONE',
  Loading = 'LOADING',
  Loaded = 'LOADED'
}

export type DenseArgs = {
   __typename?: 'DenseArgs',
  activation?: Maybe<Scalars['String']>,
  units?: Maybe<Scalars['Int']>,
};

export type DropoutArgs = {
   __typename?: 'DropoutArgs',
  rate?: Maybe<Scalars['Int']>,
};

export type InputConv2dArgs = {
   __typename?: 'InputConv2dArgs',
  activation?: Maybe<Scalars['String']>,
  filters?: Maybe<Scalars['Int']>,
  kernelSize?: Maybe<Scalars['Int']>,
  padding?: Maybe<Scalars['String']>,
  inputShape?: Maybe<Array<Scalars['Int']>>,
};

export type Layer = {
   __typename?: 'Layer',
  id: Scalars['ID'],
  name: Scalars['String'],
  layerType: LayerType,
  layerArgs?: Maybe<LayerArgs>,
};

export type LayerArgs = Conv2dArgs | InputConv2dArgs | DenseArgs | MaxPool2DArgs | DropoutArgs;

export enum LayerType {
  Conv2D = 'CONV2D',
  InputConv2D = 'INPUT_CONV2D',
  DenseOutput = 'DENSE_OUTPUT',
  DenseHidden = 'DENSE_HIDDEN',
  MaxPool_2D = 'MAX_POOL_2D',
  Dropout = 'DROPOUT',
  Flatten = 'FLATTEN'
}

export type MaxPool2DArgs = {
   __typename?: 'MaxPool2DArgs',
  poolSize?: Maybe<Scalars['Int']>,
  strides?: Maybe<Scalars['Int']>,
};

export enum MessageAlertType {
  Success = 'SUCCESS',
  Info = 'INFO',
  Warning = 'WARNING',
  Error = 'ERROR'
}

export enum MessageType {
  Error = 'ERROR',
  ModelStateError = 'MODEL_STATE_ERROR',
  ModelStateChange = 'MODEL_STATE_CHANGE',
  Alert = 'ALERT'
}

export enum ModelState {
  ModelNone = 'MODEL_NONE',
  Created = 'CREATED',
  Preloaded = 'PRELOADED',
  Ready = 'READY',
  Executing = 'EXECUTING'
}

export type Mutation = {
   __typename?: 'Mutation',
  registerUser: Scalars['Boolean'],
  authenticateUser: Scalars['String'],
  createCat?: Maybe<Cat>,
  createLayer: Layer,
  createModel: Scalars['Boolean'],
  loadModel: Scalars['Boolean'],
  popLayer: Scalars['Boolean'],
  loadDataset: Scalars['Boolean'],
  compileModel: Scalars['Boolean'],
  predict: Scalars['Boolean'],
  evalute: Scalars['Boolean'],
  train: Scalars['Boolean'],
  stopTraining: Scalars['Boolean'],
  saveModel: Scalars['Boolean'],
  resetModel: Scalars['Boolean'],
  updateUser?: Maybe<User>,
  updateSelf?: Maybe<User>,
  deleteUser?: Maybe<User>,
};


export type MutationRegisterUserArgs = {
  dto?: Maybe<CreateUserInput>
};


export type MutationAuthenticateUserArgs = {
  dto?: Maybe<SignUserInput>
};


export type MutationCreateCatArgs = {
  createCatInput?: Maybe<CreateCatInput>
};


export type MutationCreateLayerArgs = {
  createLayerInput?: Maybe<CreateLayerInput>
};


export type MutationLoadModelArgs = {
  modelPath?: Maybe<Scalars['String']>
};


export type MutationLoadDatasetArgs = {
  setNumber?: Maybe<Scalars['Int']>
};


export type MutationSaveModelArgs = {
  modelPath?: Maybe<Scalars['String']>
};


export type MutationUpdateUserArgs = {
  dto: UpdateUserInput,
  id: Scalars['ID']
};


export type MutationUpdateSelfArgs = {
  dto: UpdateUserInput
};


export type MutationDeleteUserArgs = {
  id: Scalars['ID']
};

export type NetworkMessage = {
   __typename?: 'NetworkMessage',
  eventName: Scalars['String'],
  type?: Maybe<MessageAlertType>,
  error?: Maybe<Scalars['String']>,
  stateType?: Maybe<Scalars['String']>,
  prevState?: Maybe<State>,
  state?: Maybe<State>,
  nextState?: Maybe<State>,
  message?: Maybe<Scalars['String']>,
};

export type Query = {
   __typename?: 'Query',
  getCats?: Maybe<Array<Maybe<Cat>>>,
  cat?: Maybe<Cat>,
  layers?: Maybe<Array<Layer>>,
  layer_args: LayerArgs,
  users?: Maybe<Array<User>>,
  user?: Maybe<User>,
  self?: Maybe<User>,
};


export type QueryCatArgs = {
  id: Scalars['ID']
};


export type QueryUserArgs = {
  id: Scalars['ID']
};

export enum RoleType {
  Admin = 'ADMIN',
  User = 'USER'
}

export type SignUserInput = {
  email: Scalars['String'],
  password: Scalars['String'],
};

export type State = {
   __typename?: 'State',
  model?: Maybe<ModelState>,
  data?: Maybe<DataState>,
};

export type Subscription = {
   __typename?: 'Subscription',
  catCreated?: Maybe<Cat>,
  networkMessage?: Maybe<NetworkMessage>,
  networkTraining?: Maybe<TrainingLog>,
};

export type TrainingLog = {
   __typename?: 'TrainingLog',
  eventName: Scalars['String'],
  type: Scalars['String'],
  batch?: Maybe<Scalars['Int']>,
  size?: Maybe<Scalars['Int']>,
  loss?: Maybe<Scalars['Float']>,
  acc?: Maybe<Scalars['Float']>,
  val_loss?: Maybe<Scalars['Float']>,
  val_acc?: Maybe<Scalars['Float']>,
};

export type UpdateUserInput = {
  name?: Maybe<Scalars['String']>,
  password?: Maybe<Scalars['String']>,
};

export type User = {
   __typename?: 'User',
  id: Scalars['ID'],
  email: Scalars['String'],
  name: Scalars['String'],
  roles?: Maybe<Array<UserRole>>,
};

export type UserRole = {
   __typename?: 'UserRole',
  id: RoleType,
  name: Scalars['String'],
};

export type AuthenticateUserMutationVariables = {
  email: Scalars['String'],
  password: Scalars['String']
};


export type AuthenticateUserMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'authenticateUser'>
);

export type RegisterUserMutationVariables = {
  name: Scalars['String'],
  email: Scalars['String'],
  password: Scalars['String']
};


export type RegisterUserMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'registerUser'>
);


export const AuthenticateUserDocument = gql`
    mutation AuthenticateUser($email: String!, $password: String!) {
  authenticateUser(dto: {email: $email, password: $password})
}
    `;
export type AuthenticateUserMutationFn = ApolloReactCommon.MutationFunction<AuthenticateUserMutation, AuthenticateUserMutationVariables>;
export type AuthenticateUserComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<AuthenticateUserMutation, AuthenticateUserMutationVariables>, 'mutation'>;

    export const AuthenticateUserComponent = (props: AuthenticateUserComponentProps) => (
      <ApolloReactComponents.Mutation<AuthenticateUserMutation, AuthenticateUserMutationVariables> mutation={AuthenticateUserDocument} {...props} />
    );
    
export type AuthenticateUserProps<TChildProps = {}> = ApolloReactHoc.MutateProps<AuthenticateUserMutation, AuthenticateUserMutationVariables> & TChildProps;
export function withAuthenticateUser<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  AuthenticateUserMutation,
  AuthenticateUserMutationVariables,
  AuthenticateUserProps<TChildProps>>) {
    return ApolloReactHoc.withMutation<TProps, AuthenticateUserMutation, AuthenticateUserMutationVariables, AuthenticateUserProps<TChildProps>>(AuthenticateUserDocument, {
      alias: 'authenticateUser',
      ...operationOptions
    });
};

/**
 * __useAuthenticateUserMutation__
 *
 * To run a mutation, you first call `useAuthenticateUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAuthenticateUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [authenticateUserMutation, { data, loading, error }] = useAuthenticateUserMutation({
 *   variables: {
 *      email: // value for 'email'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useAuthenticateUserMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<AuthenticateUserMutation, AuthenticateUserMutationVariables>) {
        return ApolloReactHooks.useMutation<AuthenticateUserMutation, AuthenticateUserMutationVariables>(AuthenticateUserDocument, baseOptions);
      }
export type AuthenticateUserMutationHookResult = ReturnType<typeof useAuthenticateUserMutation>;
export type AuthenticateUserMutationResult = ApolloReactCommon.MutationResult<AuthenticateUserMutation>;
export type AuthenticateUserMutationOptions = ApolloReactCommon.BaseMutationOptions<AuthenticateUserMutation, AuthenticateUserMutationVariables>;
export const RegisterUserDocument = gql`
    mutation RegisterUser($name: String!, $email: String!, $password: String!) {
  registerUser(dto: {name: $name, email: $email, password: $password})
}
    `;
export type RegisterUserMutationFn = ApolloReactCommon.MutationFunction<RegisterUserMutation, RegisterUserMutationVariables>;
export type RegisterUserComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<RegisterUserMutation, RegisterUserMutationVariables>, 'mutation'>;

    export const RegisterUserComponent = (props: RegisterUserComponentProps) => (
      <ApolloReactComponents.Mutation<RegisterUserMutation, RegisterUserMutationVariables> mutation={RegisterUserDocument} {...props} />
    );
    
export type RegisterUserProps<TChildProps = {}> = ApolloReactHoc.MutateProps<RegisterUserMutation, RegisterUserMutationVariables> & TChildProps;
export function withRegisterUser<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  RegisterUserMutation,
  RegisterUserMutationVariables,
  RegisterUserProps<TChildProps>>) {
    return ApolloReactHoc.withMutation<TProps, RegisterUserMutation, RegisterUserMutationVariables, RegisterUserProps<TChildProps>>(RegisterUserDocument, {
      alias: 'registerUser',
      ...operationOptions
    });
};

/**
 * __useRegisterUserMutation__
 *
 * To run a mutation, you first call `useRegisterUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRegisterUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [registerUserMutation, { data, loading, error }] = useRegisterUserMutation({
 *   variables: {
 *      name: // value for 'name'
 *      email: // value for 'email'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useRegisterUserMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<RegisterUserMutation, RegisterUserMutationVariables>) {
        return ApolloReactHooks.useMutation<RegisterUserMutation, RegisterUserMutationVariables>(RegisterUserDocument, baseOptions);
      }
export type RegisterUserMutationHookResult = ReturnType<typeof useRegisterUserMutation>;
export type RegisterUserMutationResult = ApolloReactCommon.MutationResult<RegisterUserMutation>;
export type RegisterUserMutationOptions = ApolloReactCommon.BaseMutationOptions<RegisterUserMutation, RegisterUserMutationVariables>;