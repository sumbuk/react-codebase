import React from 'react';
import { Grid, Row, Col } from 'react-flexbox-grid';
import { useBlueprint } from '../providers';

const BaseLayout: React.FC = ({ children }) => {
  const { theme } = useBlueprint();
  return (
    <Grid fluid className={theme}>
      <Row>
        <Col xs>{children}</Col>
      </Row>
    </Grid>
  );
};

export default BaseLayout;
