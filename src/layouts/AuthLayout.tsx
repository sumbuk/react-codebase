import React from 'react';
import { Grid, Row, Col } from 'react-flexbox-grid';
import { useBlueprint } from '../providers';

const AuthLayout: React.FC = ({ children }) => {
  const { theme } = useBlueprint();
  return (
    <Grid className={theme} fluid>
      <Row center="xs" middle="xs" className='full-height'>
        <Col xs={10} sm={8} md={5}>
          {children}
        </Col>
      </Row>
    </Grid>
  );
};

export default AuthLayout;
