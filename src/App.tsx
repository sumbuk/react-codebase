import React from 'react';
import { Switch, Redirect, Route } from 'react-router-dom';
import Welcome from './views/Welcome';
import Home from './views/Home';
import { routes } from './router';
import { useToken } from './hooks';
import Login from './views/Login';
import Register from './views/Register';
import './App.css';

const matchHomeComponent = (token?: string) => (token ? Home : Welcome);
const matchLoginComponent = (token?: string) =>
  token ? () => <Redirect to={routes.home} /> : Login;
const matchRegisterComponent = (token?: string) =>
  token ? () => <Redirect to={routes.home} /> : Register;

const App: React.FC = () => {
  const token = useToken();
  return (
    <Switch>
      <Route exact path={routes.login} component={matchLoginComponent(token)} />
      <Route exact path={routes.register} component={matchRegisterComponent(token)} />
      <Route exact path={routes.home} component={matchHomeComponent(token)} />
      <Redirect to={routes.home} />
    </Switch>
  );
};

export default App;
