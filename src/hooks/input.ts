import { useState } from 'react';
import { Intent } from '@blueprintjs/core';

export const useInput = (initialValue: string, initialValidationText: string = '') => {
  const [value, setValue] = useState(initialValue);
  const [validationText, setValidationText] = useState(initialValidationText);
  const intent = validationText ? Intent.DANGER : Intent.NONE;
  return {
    value,
    setValue,
    validation: {
      intent,
      text: validationText,
      setter: setValidationText
    },
    reset: () => {
      setValue('');
      setValidationText('');
    },
    bind: {
      intent,
      value,
      onChange: (event: any) => {
        setValue(event.target.value);
      }
    }
  };
};
