import { useMemo } from 'react';
import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';

export const useToken = () => {
  const { data } = useQuery(gql`
    query {
      token @client
    }
  `);
  return useMemo(() => {
    return data && data.token ? data.token : '';
  }, [data]);
};
