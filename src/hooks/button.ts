import React from 'react';

export const useButton = (
  callback: () => void,
  deps: any[],
  text: string,
  icon: string,
  isMinimal: boolean = true
) => {
  const handler = React.useCallback(callback, deps);
  const bindButton = React.useMemo(
    () => ({
      onClick: handler,
      minimal: isMinimal,
      icon: icon as any,
      text
    }),
    [handler, text, icon, isMinimal]
  );

  return bindButton;
};
